class Game {

    private int score;
    private int last_score;
    private boolean isStrike=false;
    private boolean isStrike2=false;
    private boolean isSpare=false;

    public Game (){
        score=0;
        last_score=0;;
    }

    public void roll(int pins){
        score+=pins; // Aggiorno lo score con i birilli buttati giu in questo tentativo

        if(isSpare){ // Raddoppio il punteggio del primo tentativo successivo (dopo aver fatto Spare)
            score+=pins;
            isSpare=false;
        }

        else if(isStrike){ // Raddoppio il punteggio del primo tentativo successivo (dopo aver fatto Strike)
            score+=pins;
            isStrike=false;
            isStrike2=true;
        }

        else if(isStrike2){ // Raddoppio il punteggio del secondo tentativo successivo (dopo aver fatto Strike)
            score+=pins;
            isStrike2=false;
        }

        if(last_score+pins==10){ // Se ho fatto Spare
            isSpare=true;
        }

        else if(last_score==10){ // Se ho fatto Strike
            isStrike=true;

        }


        last_score=pins; // Salvo l'ultimo score

    }
    public int score(){
        return score;
    }
}
